from . import joint

class skeleton(Object):
	"""Skeleton class contains the whole iinformation in the bvh file"""
	def __init__(self):
		"""Initialize"""
		self.joints = []
		self.nodes = 0
		self.total_channels = 0
		self.chainends = 0
		self.frames = 0
		self.frame_time = 0
		
		return True

	def add_joint(self):
		"""Initialize"""
		self.joints.append(new joint())
		
		return True

	def calculate_parameters(self, frame, frame_time):
		"""calculate parameters"""
		self.nodes = len(joints)
		for i in xrange(0, self.nodes-1):
			self.total_channels += self.joints[i].Nchannels
			if self.joints[i].Nchannels == 0:
				chainends += 1

		self.frames = frame
		self.frame_time = frame_time
		
		return True